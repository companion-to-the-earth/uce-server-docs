# UCE Server Documentation

This server is used for the [Utrecht Companion to the Earth](https://softwaregameprojecten.sites.uu.nl/2020/06/26/utrecht-companion-to-the-earth/).

The main purpose for this service is to connect the teacher module and the app.
For instance, pins created by a teacher in the teacher module are saved on the
server. These pins can then be downloaded by the app.
Authentication for both the teacher module and the app is also handled through
this server.

## Documentation

For documentation on the server itself, see the [architecture](overview.md)
section. This section contains information on all the services.

Additionally, documentation on the CI/CD pipeline can be found
in the [pipeline](pipeline.md) section.

## Setup
For instructions on how to set the server up, see the [installation guide](guide.md).
This includes instructions for both a production and development setup.
