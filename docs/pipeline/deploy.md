# Deploy

This job is only used in the server project.

After building the production images, this job deploys the tagged version of the
server.

If a predeploy script is provided in `.config/predeploy.sh` this is executed
first. The repository being deployed is cloned in `$CLONE_PATH`, so any files
required to start the server have to be copied from this path.

After the predeploy script, docker-compose is used to pull all required production
images and start the server.