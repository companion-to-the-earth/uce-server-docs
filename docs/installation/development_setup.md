# Development setup

To start the server locally a `docker-compose.dev.yml` is provided.
Throughout working on the project, you should make sure `docker-compose.yml`
and `docker-compose.dev.yml` stay in sync and updated.

The main difference between these files is that the dev file uses
`build` instead of `image` so all docker images are built locally instead of
using the prebuilt images from the gitlab container registry.

## Extra dependencies

While not necessarily required to set up the server, it is recommended to
install these as it will allow you to use the Makefile to rebuild
requirement files and schema files.

These dependencies are checked when running the Makefile so you will be warned
if any are missing.

### pip-tools

[pip-tools](https://pypi.org/project/pip-tools/) is used to rebuild 
requirements.txt files from requirements.in files.
This allows us to pin dependency versions.

To install, run:
```
$ pip install pip-tools
```

### BARE

[BARE](https://pypi.org/project/bare/) is used to build .py files to
encode/decode BARE messages from a BARE schema file.

To install, run:
```
$ pip install bare
```

## Setup

To start the server, run:
```
$ docker-compose -f docker-compose.dev.yml up --build -d
``` 

If you want to view the logs, see:
```
$ docker-compose logs --help
```

To stop the server, run:
```
$ docker-compose down
```

### Configuration

The default .env file is already set up for use in development.
However, feel free to make changes if needed.

Enabling certbot in development will not work unless you have a hostname where your local server is reachable.

To debug mails you don't need to configure anything.
The console backend will be used by Flask-Mail in development so all mails are printed to the server logs.
To view (and follow) this you can use:
```
$ docker-compose logs -f mail
```

### Rebuilding changes

After making changes to a service the server needs to be restarted.
To do this, simply run the same command you used to start the server again:
```
$ docker-compose -f docker-compose.dev.yml up --build -d
```
The --build flag ensures the images are rebuilt instead of reusing the existing images.
Because Docker caches a lot, rebuilding the images is a lot faster than initial startup.

### Database reset

If you need to reset the database for some reason, just delete the `db-auth/data`
and `db/data` directories.
```
$ rm -rf db-auth/data db/data
```

Make sure the server is off before doing this, afterwards when the server is restarted
both databases will be rebuilt.

### Remove all Docker images

If you're having issues with Docker or you are running out of space because of many
unused images, you can remove all unused containers/images/networks by running:
```
$ docker system prune -a
```
