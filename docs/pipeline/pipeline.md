# CI/CD Pipeline

The CI/CD pipeline can be found in the
[continuous-devlivery](https://gitlab.com/companion-to-the-earth/continuous-delivery)
repository.
The pipeline is shared between multiple projects.
This documentation is mostly about what the different stages do, for information
on how to set it up, see the installation guide.

## Stages
The pipeline consists of several stages. Here's an overview of all the stages
currently defined. Note that not all stages will be executed for each pipeline,
there is some configuration possible to enable/disable certain stages per project.
Additionally, there are rules that specify when to run a certain stage.

Each of these stages can generate some artifacts required by another stage.
This is all handled by gitlab and doesn't need to be done manually. For example:
The trigger stage creates a list of changed files called `changedfiles.txt`.
This file is later automatically downloaded in the prepare stage and can be used
there.

### [Trigger](trigger.md)
Checks which files were changed to determine which images to rebuild.

### [Prepare](prepare.md)
Gets a list of images to rebuild.

### [Verify](verify.md)
Verifies repository structure.

### [Build](build.md)
Builds all the needed test images.

### [Test_unit](test_unit.md)
Runs unit tests.

### [Test_integration](test_integration.md)
Runs integration tests.

### [Test_end2end](test_end2end.md)
Runs end to end tests.

### [Predeploy](predeploy.md)
Generates documentation, checks codestyle, checks test coverage and, if required,
builds production images.

### [Deploy](deploy.md)
Automatically deploys the server.

### [Package](package.md)
Creates a python package and uploads it to the gitlab package registry.

## Usage
There are several gitlab CI YAML files you can import to use the pipeline.
For the server we use `.gitlab-ci.default.yml` with some extra files for tests
and automatic deploys.

Here's the `.gitlab-ci.yml` file for the server.
```
variables:
  CD_VERSION: &version '4.1.2'
  CD_PROJECT: &project 'companion-to-the-earth/continuous-delivery'

image: registry.gitlab.com/$CD_PROJECT/cd:$CD_VERSION

before_script:
  # Get ci-cd scripts of specified CD_VERSION
  - |
    [ -d cd ] &&
       rm -rf cd

    CLONE_URL="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/$CD_PROJECT.git"
    git clone \
       --single-branch \
       --branch "$CD_VERSION" \
       "$CLONE_URL" \
       ci-cd
    mv ci-cd/cd cd

    rm -rf ci-cd

# choose which parts of the pipeline to include
include:
    # default pipeline base
  - project: *project
    ref: *version
    file: '.gitlab-ci.default.yml'

    # include unit and integration tests
  - project: *project
    ref: *version
    file: '.gitlab-ci.tests.yml'

    # include end to end tests
  - project: *project
    ref: *version
    file: '.gitlab-ci.test.end2end.yml'

    # include deploy step
  - project: *project
    ref: *version
    file: '.gitlab-ci.deploy.yml'
```

As you can see the unit test, integration test, end to end test and deploy steps
are not included in the default config. This is because only the server needs to
be deployed and only the server has end to end tests. Splitting tests, deployment
and packaging into separate files makes the pipeline more flexible to be
configured depending on what the specific project needs.

Another example which includes packaging is the configuration used in
[lib-database-connector](https://gitlab.com/companion-to-the-earth/lib-database-connector).
```
variables:
  CD_VERSION: &version '3.1.1'
  CD_PROJECT: &project 'companion-to-the-earth/continuous-delivery'

image: registry.gitlab.com/$CD_PROJECT/cd:$CD_VERSION

before_script:
  # Get ci-cd scripts of specified CD_VERSION
  - |
    [ -d cd ] &&
       rm -rf cd

    CLONE_URL="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/$CD_PROJECT.git"
    git clone \
       --single-branch \
       --branch "$CD_VERSION" \
       "$CLONE_URL" \
       ci-cd
    mv ci-cd/cd cd

    rm -rf ci-cd

include:
  - project: *project
    ref: *version
    file: '.gitlab-ci.default.yml'
  - project: *project
    ref: *version
    file: '.gitlab-ci.tests.yml'
  - project: *project
    ref: *version
    file: '.gitlab-ci.package.yml'
```

Finally, there is a file called `.gitlab-ci.basic.yml` which is used by
[wsgi-server](https://gitlab.com/companion-to-the-earth/wsgi-server).
This configuration only checks the repository structure and builds the required
images, without any tests or coverage checks. The reason this configuration
exists is because wsgi-server is a very small project that can't really be tested,
as its only purpose is to start a flask service.