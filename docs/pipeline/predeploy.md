# Predeploy

This is the final stage for most pipelines.
Several jobs run in this stage, depending on the situation.

## Codestyle

This job runs pylint to get a codestyle report for each service.
If the score is too low, the job fails.
The detailed pylint report can be found in the artifacts.

## Generate docs

This job uses pydoc to generate documentation from all the docstrings in the
code.
For this to work you can't use relative imports, so always import like this:
```
from src.process import Auth
```
Instead of:
```
from .process import Auth
```
Even though this may show up as an error in your IDE/text editor.

The generated documentation is uploaded as an artifact.

## Coverage report

This job uses the previously generated `.coverage` files to determine the
test coverage and checks if it's above a certain threshold.
If not, the job will fail.

The full coverage report is visible in the job output so you know which lines
in which files are not covered and what to do to increase coverage.

## Build prod

This job build the production images. It is only executed for release tags.
As said before, the production images are the same as the test images, except
dependencies only used in tests are not installed.