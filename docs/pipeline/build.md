# Build

This is the first build stage in the pipeline, the second one only builds
production images and only runs on tags.

This build stage builds the test images, which are pretty much identical to the
production images with the exception that dependencies which are only required
for testing are not installed in the production images.

It uses the list generated in the prepare stage. Every image is tagged with
`<ref name>-<stage>`, so the auth image on the develop branch would be
`auth:develop-test`.

After building the images are uploaded to the gitlab container registry so they
are available to all other stages.

!!!warning
    Sometimes uploading can fail and an old image is used in the tests.
    If your tests are failing for some unknown reason, check the build stage
    and retry it if there were any errors.