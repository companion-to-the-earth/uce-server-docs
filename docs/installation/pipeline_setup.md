# Pipeline setup

## Setup

It is recommended to use two servers for the full pipeline.
One server will run the server, the other will be used to run all the 
tests and build the images.

To do this we use multiple gitlab runners. The gitlab runner running on the production
is indicated by a `production` tag.

## Extra requirements

These extra dependencies should be installed on all servers where gitlab runners will be set up.

### Install git

Install git using the official [installation instructions](https://git-scm.com/download/linux).

!!! warning
    For RHEL and derivatives extra steps are required to get the latest version.
    These extra steps are described below. Using the wrong git version will cause
    the gitlab pipeline to fail.

As noted on the git website, RHEL and derivatives typically ship older versions
of git. Either build from source or use the [IUS Community Project](https://ius.io/)
repository to get the latest version.

To install git using the IUS Community Project, first remove any existing git
package:
```
$ yum uninstall git
```

Next, follow the [IUS setup instructions](https://ius.io/setup).

Finally, install the latest git version:
```
$ yum install --exclude git git
```

Verify if the correct git version was installed:
```
$ git --version
git version 2.30.1
```
The specific version number may vary slightly.

### Install and configure gitlab-runner

Install gitlab-runner using the [official documentation](https://docs.gitlab.com/runner/install/linux-manually.html).

Once installed, verify by running:
```
$ gitlab-runner --version
```

The gitlab runner needs to be able to access docker, to do this add the
`gitlab-runner` user to the `docker` group:
```
$ usermod -aG docker gitlab-runner
```

#### Registering runners

For this project we need 4 runners in total, two group runners shared between
all repositories in the group, and two specific runners for the server.

One of these runners will have a shell executor, meaning that all commands in
the pipeline are executed on the local machine. This way the server is
automatically deployed by the pipeline.
The other runner has a docker executor which is used for testing merge requests
and commits. With a docker executor the pipeline is executed in a docker container,
not on the host system.

To set up the group runners go to the
[gitlab group](https://gitlab.com/groups/companion-to-the-earth) > settings > CI/CD > Runners.
Scroll down until you see "Set up a group runner manually", here you'll find
the token you need to register the runner.

On the build/testing server, run:
```
$ gitlab-runner register
```

and enter the following information:
```text
instance URL = https://gitlab.com/
token = <token from gitlab>
description = <descriptive name for the runner>
tags = shell,testing
executor = shell
```

Register another runner with the following information:
```text
instance URL = https://gitlab.com/
token = <token from gitlab>
description = <descriptive name for the runner>
tags = docker
executor = docker
```

Verify in gitlab that both group runners have been registered.

Next the two specific runners need to be created.
To this, go to
[uce-server](https://gitlab.com/companion-to-the-earth/uce-server) > Settings > CI/CD > Runners
and look for "Set up a specific runner manually".

Just like with the group runners, register another runner:
```text
instance URL = https://gitlab.com/
token = <token from gitlab>
description = <descriptive name for the runner>
tags = docker
executor = docker
```

Finally, set up the production shell runner.
This should be set up on the production server.
```text
instance URL = https://gitlab.com/
token = <token from gitlab>
description = <descriptive name for the runner>
tags = shell,production
executor = shell
```
This runner only runs the deploy step.

## Deploying the server using the pipeline

Once the pipeline is set up, the server can be automatically deployed using the pipeline.
You can either release and deploy a new version or deploy an existing server version.

After the deploy job is finished, the server should be online.
You can verify this by making a GET request to `/api/status`, the version field in the response should match the tag name.

### Create new release

If you have some features merged into the develop branch that you would like to release,
you can merge this branch into master.

Once this is complete, [create a tag](https://docs.gitlab.com/ee/user/project/releases/#add-release-notes-to-git-tags).
Make sure you are creating this tag from the `master` branch!
The title for this tag should be the version number, we use [semantic versioning](https://semver.org/)
so ensure the version number is consistent with this format.

You should add release notes to the tag, the format for these messages is:
```
# <version number>

### Features
* <list of new features>

### Fixes
* <list of bugfixes>
```

Here's an example:
```
# 1.2.0

### Features
* Added password reset procedure
* Added content library API
```

Adding release notes is required so Gitlab creates a release for the tag.

Once the tag is created, go to the pipelines overview to verify that a pipeline has been started for the tag.
This should include jobs to build production images and deploy the server.

### Deploy existing version

Go to the [pipelines overview](https://gitlab.com/companion-to-the-earth/uce-server/-/pipelines)
in gitlab and press the "Run pipeline" button in the top right.
Select the tag you want to run the pipeline for. Note that the deploy job is only added for tag
pipelines so you can't deploy a specific branch.

### Note for first automatic deploy

If this is the first time you deploy the server using the pipeline the deploy step will fail because there is no .env file.
Log into the server and go to `/home/gitlab-runner/builds/<id>/<id>/companion-to-the-earth/uce-server`.
Create a .env file and paste in the template provided in the repository.
Configure the server using the guide in [server setup](http://localhost:8000/uce-server-docs/installation/server_setup/#configuration),
make sure to do the initial database configuration as well *after* starting the server.
Now, restart the deploy job and it should be successful.

This extra step is only necessary when deploying the first time or if the .env file changed.
