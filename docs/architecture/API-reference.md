# API Reference

The OpenAPI specification can be found
[here](https://gitlab.com/companion-to-the-earth/uce-server/-/blob/master/openapi3.yml).