# db-setup

This is a container responsible for setting up the main database.
The reason this was put in a seperate container is because the database is shared
between multiple services so it wouldn't make sense to make this a responsibility
for one of the existing services.

Database migrations are handled with Alembic, as discussed in the
[overview](overview.md).

To change the schema, edit the `models.py` file and generate a migration using:
```
bin/generate_migration.sh db-setup "[migration message]"
```
Note that this compares the models with what's currently in the database so to
make sure the migration is generated correctly it might be a good idea to reset
the database first. This can be done by removing the `db/data` directory.