# Prepare

This stage checks which images need to be rebuilt using the changedfiles list
generated in the previous stage.

There are some files that will always trigger a full rebuild if changed:

* .gitlab-ci.yml
* docker-compose.yml
* .env

For other files it checks the `.buildignore` file, which lists files that
shouldn't trigger a rebuild. Then it checks the path of the changed file and
checks if this path contains an image by checking for a Dockerfile.

Once there is a list of images to rebuild it also creates lists of images with
unit tests and integration tests for the other stages.
These are used to determine which tests to run.
