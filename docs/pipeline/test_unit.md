# Test_unit

This stage runs the unit tests.
This is done by copying the `unit_run.sh` script into the container of the service
to test. This script uses [coverage](https://pypi.org/project/coverage/) to
run all `utest_*.py` files and collects coverage information.

The `.coverage` file is then copied out of the container and uploaded to gitlab
as an artifact. This coverage file will be combined with the coverage files of
the other test stages and checked in the predeploy stage.

## Writing unit tests

For testing the [unittest](https://docs.python.org/3/library/unittest.html)
library is used.
To add unit tests to a service, create a `test_unit` directory and make sure it
is copied into the image in the Dockerfile.
Then add test code in `utest_*.py` files in the directory and they should be
executed in the pipeline.