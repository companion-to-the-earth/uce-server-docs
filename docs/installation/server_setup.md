# Server setup

Setting up a specific version of the server can be done quite easily.

First, clone the server repository.

## Configuration

Before starting the server some things need to be configured.
This can be done by editing the `.env` file:

### Database

The database configuration doesn't need to change too much, though you might want to choose more secure passwords.

### JWT Key

`JWT_KEY` is used to sign the JWTs so it should be set to something secure.

### HTTPS config

Certbot can be used to automatically request and renew TLS certificates.
To use this, set `USE_CERTBOT` to 1.
It is highly recommended to set up HTTPS when running the server.

Additionally, to use certbot `DOMAIN_NAME` needs to be set to the server's domain name.
This will also be the subject name on the requested certificate.

To configure which ports will be exposed to the outside world, use `PROXY_PORT` and `PROXY_PORT_SSL`.
For production, you'll most likely want port 80 and 443 respectively.

`CERTBOT_EMAIL` should be set to a valid email address when using certbot.
While this is not required, it is highly recommended as this allows letsencrypt to send alerts if necessary.


### Mail config

To configure which SMTP server to use for sending mails, set `MAIL_SERVER` and `MAIL_PORT`.
If the server requires authentication, set `MAIL_USERNAME` and `MAIL_PASSWORD`


For security, set either `MAIL_USE_TLS` **or** `MAIL_USE_SSL` to 1.
These are mutually exclusive, setting both will not work. TLS is the default and is recommended.

`MAIL_DEFAULT_SENDER` is the address that will appear as the sender for all mails sent by the server.
Set it to something appropriate.

### Version

This is only required when setting up the server without the CI/CD pipeline.
Add the following line to the .env file:
```
CI_COMMIT_REF_NAME=<ref name>
```

The ref name can be a version number (see [releases](https://gitlab.com/companion-to-the-earth/uce-server/-/releases))
or a branch name.

### Database

Unfortunately, there are some intial things that need to be set up in the database which aren't automated yet.
Before using the server a university and an admin user need to be created, after that everything can be done
through the teacher module.

Note that these steps can only be completed once the server is running.
This only has to be done once after he first startup, or if the database was reset.
It is not necessary when setting up a development server as test data will automatically be inserted there.

#### University setup

To create a university, log into the main database using:
```
$ docker exec -it <container name> mysql -p
```

The container name can be found using `docker ps`, it is most likely `uce-server_db_1`.
The password is the root password set in the .env file.

Next, connect to the database:
```
connect db-server;
```

Finally, add a university by running the following:
```
INSERT INTO universities VALUES (10000, "<university name>");
```

Exit using:
```
exit;
```

#### Create admin user

Next the admin user needs to be created, for this a script has been provided in the `bin/` folder.


Run the script like so, script output is indicated with '>':
```
$ python bin/create_user.py

> WARNING: DO NOT RUN THIS IN PRODUCTION
> The intended usage is to add a user in a testing environment
> Are you sure you want to continue? (y/n)

y

> Email:

<enter desired email address>

> Password:

<enter password for admin account, this won't show up in the console>

> Name:

<account display name, "admin" recommended>

> Authorization levels:
>     1. student
>     2. teaching assistant
>     3. teacher
>     4. admin
> Authorization level:

4 (REQUIRED FOR ADMIN ACCOUNT)

> Container ID: <container id>
> Query: insert into users(id, email, password, name, pin_group_id, auth_level) values ('2927568095087050969','test@example.com', '$2b$12$ffoZgh114heM.luh0XLs9u2pNTqFi8btTCL4AUCd9g1/Qg37wzLzS', 'admin', '-1', '4');
> Run this query? (y/n)

y (verify that the query is correct and the container id matches the id of the auth database container)
```

After these steps you can log in as the admin user.

## Setup

After everything is configured, start the server using:
```
$ docker-compose up -d
```
Docker compose should start all the services and after this the server is online.

To verify that all services are running, use:
```
$ docker-compose ps
```

If db-setup is not running, this is fine. This container only runs on startup to set up the database.

To view the logs, see:
```
$ docker-compose logs --help
```

To stop the server, run:
```
$ docker-compose down
```
