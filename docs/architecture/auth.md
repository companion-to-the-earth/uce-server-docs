# auth

This service handles user authentication and authorization. It is the only
service with its own database: [db-auth](db-auth.md).

## Classes and files
The structure of the service is similar to most other microservices.

Here is an overview of the file structure:
```
auth
├── alembic                     # Database setup
├── Dockerfile                  # Dockerfile to build the image
├── src
│   ├── app.py                  # Flask setup
│   ├── authorization.py        # JWT generation and validation
│   ├── auth.py                 # auth blueprint
│   ├── errors.py               # API errors
│   ├── forms.py                # WTForms
│   ├── internal_api.py         # internal API implementation
│   ├── log.py                  # logging
│   ├── models.py               # database models
│   ├── process.py              # request procedures
│   ├── schemas/                # BARE schemas
│   ├── templates/              # HTML templates (used for forms)
│   ├── users.py                # users blueprint
│   └── utils.py                # utilities
├── test_integration/           # integration tests
└── test_unit/                  # unit tests
```

## Login and sessions

For most API requests an access token is needed.
There are two ways to obtain an access token:

Initially users will log in using their email and password.
This creates a new session for that user and returns an access token and a
refresh token. Active sessions can be listed and revoked through the
`/auth/sessions` endpoint. The access token has an expiration time, this is
returned along with the tokens.

Once a session has been created, a new access token can be requested by using
the refresh token. Once used, a new refresh token is generated and returned
along with the new access token.

## JSON Web Tokens

To authorise requests, JSON Web Tokens are used.
These are an industry standard way to securely represent claims between two
parties.

The token consists of a header, a payload, and a signature.
In the header the algorithm used to generate the signature is specified, for
this server HS256 is used.

The payload contains a set of claims.
The JWTs generated by this service have the following payload:

- sub: subject, user id
- iss: issuer, domain name of the server
- iat: issued at, time at which the token was generated
- exp: expires, timestamp at which the token expires
- auth_time: last time the user authenticated with email and password

The signature is used to validate the token.
It is calculated using the Base64url encoding of the header and payload and
a secret key specified in the .env file.

The library used to generate these tokens is [PyJWT](https://github.com/jpadilla/pyjwt).

For more information on JWTs see [RFC 7519](https://tools.ietf.org/html/rfc7519).

### Example

Header:
```
{
    "alg": "HS256",
    "typ": "JWT"
}
```

Payload:
```
{
    "username": "user1",
    "exp": 1516239022
}
```

Signature (using key "secretkey"):
```
HMACSHA256(
    base64UrlEncode(header) + "." + base64UrlEncode(payload),
    "secretkey"
)
```

Final token:
```
base64UrlEncode(header) + "." + base64UrlEncode(payload) + "." base64UrlEncode(signature)
= "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiZXhwIjoxNTE2MjM5MDIyfQ.cdbaBtXfnnE594HWT1AXSW64Vd5Ip979UMxKTUywdh8"
```

## Auth levels

The auth level distinguishes different types of users. This can be used for
authorization

The following auth levels are currently defined:

- auth level 1: Student
- auth level 2: Teaching assistant
- auth level 3: Teacher
- auth level 4: Admin

## Internal API

The auth service has an internal API listener running on a seperate thread, as the middleware consume function blocks until a message is received.

The following BARE schema is used for internal API requests and responses:

```
enum RequestType {
    USER_INFO_REQUEST
    CLASS_MEMBER_COUNT_REQUEST
}

enum ResponseType {
    ERROR_RESPONSE
    USER_INFO_RESPONSE
    CLASS_MEMBER_COUNT_RESPONSE
}

enum ErrorType {
    INVALID_REQUEST
    INVALID_TOKEN
    INVALID_USER_ID
}

type AuthRequest {
    request_type: RequestType,
    request_body: data
}

type AuthResponse {
    response_type: ResponseType,
    response_body: optional<data>,
    error_type: optional<ErrorType>,
    error_message: optional<string>
}

type UserInfoRequest {
    access_token: optional<string>,
    user_id: optional<i64>
}

type UserInfo {
    id: i64,
    name: string,
    email: string,
    auth_level: i32,
    class_id: optional<i64>,
    pin_group_id: i64
}

type ClassMemberCountRequest {
    class_id: i64
}

type ClassMemberCountResponse {
    member_count: i64
}
```

{%
template "internal_api.html"
name: User info request
desc: Requests user info using a JWT, used for authorization.
parameters:
    -
        name: request_type
        desc: Must be set to USER_INFO_REQUEST
    -
        name: request_body
        desc: UserInfoRequest object containing the access token or a user id
returns:
    -
        val: "UserInfo"
        when: if successful, wrapped in AuthResponse with response_type
            USER_INFO_RESPONSE
    -
        val: "AuthResponse"
        when: if token was invalid, with response_type set to ERROR_RESPONSE
            and error_type set to INVALID_TOKEN
    -
        val: "AuthResponse"
        when: if user_id was invalid, with response_type set to ERROR_RESPONSE
            and error_type set to INVALID_USER_ID
    -
        val: "AuthResponse"
        when: if request was invalid, with response_type set to ERROR_RESPONSE
            and error_type set to INVALID_REQUEST

example: |
    request = AuthRequest()
    request.request_type = RequestType.USER_INFO_REQUEST

    request_body = UserInfoRequest()
    request_body.access_token = {JWT}
    # alternatively:
    # request_body.user_id = {user id}
    request.request_body = request_body.pack()

    publish('auth', request.pack(), '{reply queue}')
%}

{%
template "internal_api.html"
name: Class member count request
desc: Requests the number of members in a class, used by group management.
parameters:
    -
        name: class_id
        desc: Class to get the member count of
returns:
    -
        val: "ClassMemberCountResponse"
        when: if successful, wrappen in AuthResponse with response type CLASS_MEMBER_COUNT_RESPONSE
    -
        val: "AuthResponse"
        when: if request was invalid, with response_type set to ERROR_RESPONSE
            and error_type set to INVALID_REQUEST
example: |
    request = AuthRequest()
    request.request_type = RequestType.CLASS_MEMBER_COUNT_REQUEST

    request_body = ClassMemberCountRequest()
    request_body.class_id = {class_id}
    request.request_body = request_body.pack()

    publish('auth', request.pack(), '{reply queue}')
%}