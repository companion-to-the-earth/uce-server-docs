# Verify

!!!warning
    The repository structure has changed a bit, this part of the pipeline
    still passes but it might not check all required files.
    This stage should be checked and edited if necessary.

This stage checks if the repository matches the expected structure for the
pipeline.

Every image should have the following structure:
```
./
└── <image name>
    ├── Dockerfile
    ├── README.md
    ├── __init__.py
    ├── src/
    |   ├── __init__.py
    |   └── example.py
    ├── test_unit/
    |   ├── __init__.py
    |   └── utest_example.py
    └── test_integration/
        ├── __init__.py
        ├── itest_example.py
        └── docker-compose.test.yml
```

Additionally, this stage lints the main docker-compose.yml used in the project.
It uses docker-compose to check for warnings or errors in the file.
