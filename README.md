# UCE Server Documentation

[companion-to-the-earth.gitlab.io/uce-server-docs](https://companion-to-the-earth.gitlab.io/uce-server-docs/)

Full documentation for
[uce-server](https://gitlab.com/companion-to-the-earth/uce-server)

Uses [mkdocs](https://www.mkdocs.org/) to build the documentation.

## Installation

To build the documentation you need both [Python3](https://www.python.org/)
and [pip](https://pypi.org/project/pip/) installed on your system.

You can check if these are installed using the following commands.
Note that the actual version numbers might be slightly different.
```
$ python --version
Python 3.9.2
$ pip --version
pip 20.3.1
```

Next, install all required packages.
```
$ pip install -r requirements.txt
```

Afterwards, verify that mkdocs is installed.
```
$ mkdocs --version
mkdocs, version 1.1.2
```

## Building

To build the current documentation:
```
$ mkdocs build
```

If you want a live preview while writing documentation you can start a server
with:
```
$ mkdocs serve
```
You can then go to `localhost:8000` in your browser to see a preview.
This will automatically refresh if you save your changes.

## Contributing

As the master branch is automatically published you should avoid committing
directly to master.
Instead, commit your changes to a new branch and create a merge request.
This way changes can be reviewed before they're published.

## Project layout

    .gitlab-ci.yml        # Gitlab CI configuration
    mkdocs.yml            # MkDocs configuration
    templates/            # Jinja templates, can be inserted on any page
    docs/
        index.md          # Documentation home page
        installation/     # All documentation related to server setup
        services/
            overview.md   # Overview of all microservices
            <service>.md  # Detailed description of a service
