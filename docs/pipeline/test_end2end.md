# Test_end2end

This stage runs the end to end tests.
For these tests, the entire server is started and the tests are run in a seperate
container. This simulates a client connecting to the server.

## Writing end to end tests

As end to end tests are already set up on the server you probably won't have to
set this up for a new project.
In the root directory of the repository there is a `test_end2end` directory.
This directory contains a `Dockerfile` to build the end to end test image and
a `docker-compose.test.yml` to start it. Test files are mounted as a volume,
just like with the integration tests. Tests can be written in `etest_*.py` files.