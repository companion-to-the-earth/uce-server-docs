# Overview

The server uses a microservice architecture.
This means that it is split up into several services with one specific
responsibility to ensure extensibility.
Some of these microservices expose a public API while others are just used
internally.
The services can communicate internally via AMQP using the middleware.

## Services
### [proxy](proxy.md)
Reverse proxy, handles all incoming requests and sends them to the
appropriate service.

### [auth](auth.md)
Handles user authentication and authorization. Also handles all user account
endpoints (create account/change user data).

### [db-setup](db-setup.md)
Database setup, runs on startup then exits.

### [db](db.md)
Database storing pins, groups, maps, etc.

### [db-auth](db-auth.md)
Database used by the auth service.

### [middleware-db](middleware-db.md)
Internal service used to execute queries on the database.

### [group management](management.md)
Handles university, class, course and user-group requests.

### [pins](pins.md)
Handles pin API requests.

### [mail](mail.md)
Internal service used to send mails

## Docker

To ensure portability, each of these services run in their own docker container.
At the root of the project is a `docker-compose.yml` which specifies all services
that need to be started and which ports to expose.

Docker compose automatically creates a virtual network for these containers
and the only port that gets exposed to the host machine is 8080, which is
mapped to port 80 on the proxy container.
Every Flask app that exposes a public API listens for requests on port 5000,
redirecting to the service is done by the proxy.

The docker images are pulled from the gitlab repository, the CI/CD pipeline
automatically builds the images for each release.
By specifying a version number in the `.env` file you can choose which version
of the server you want to deploy. More information about this can be found in
the [installation guide](guide.md)

## Middleware

The middleware is used for internal communication between the services.
This is done using the [lib-middleware-connector](https://gitlab.com/companion-to-the-earth/lib-middleware-connector).
This library abstracts everything with connections, channels and queues
so anything using this library only has to worry about publishing and
consuming messages.
Messaging is done over [AMQP](https://en.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol)
with [RabbitMQ](https://www.rabbitmq.com/) as the message broker.
The message broker also runs in its own docker container.
RabbitMQ is a very popular and widely used message broker. A message broker is
software that enables applications, systems and services, to communicate
with each other and exchange information. The message broker makes the messaging
system asynchronous so applications don't have to wait for each other until
others have received their message.
The message broker saves messages in a queue.

![](../images/rabbitmq_explained.png)

## BARE encoding

!!!note
    This way can still be used to communicate between services.
    However, it is easier to define internal API endpoints and use
    HTTP for internal communication.
    Then the middleware can eventually be removed, making the server
    simpler and easier to maintain.

For middleware communication [BARE Message Encoding](https://baremessages.org/)
is used.
This is a simple binary representation for structured data optimized for small
messages.

Each service that exposes an internal API has a `schemas` directory with .bare
files. These files use the BARE DSL to specify the message schemas and allow
code generation. From these schema files python files are generated containing all the necessary code to pack and unpack the specified structures.

If two services need to communicate via the middleware, a schema file needs to
be shared so both sides know how to pack and unpack the messages. To achieve
this symbolic links are used.

For example: The **auth** service exposes an internal API, part of this api is
a schema file `auth.bare`, describing the format of request and response
messages. Now we have another service, **service1**, that needs to use this
API.
To share the schema file we can create a symlink:
```
$ ln -rs auth/src/schemas/auth.bare service1/src/schemas/auth.bare
```
Make sure you pass the `-rs` switch for a `relative` path and a `symbolic`
link.

Now **service1** has the same schema file which can be compiled using the
makefile.
Information on how to use this can be found in the
[README](https://gitlab.com/companion-to-the-earth/uce-server/-/blob/master/README.md).

## Databases

For the main database MySQL is used. For the auth database we switched to
PostgreSQL because MySQL does not support transactional DDL statements. This is
important for the migrations as we need to be able to roll back failed
migrations. Additionally, PostgreSQL has more sane defaults like using
**UTF-8** instead of **latin1-swedish** for text encoding.

### Migrations

[Alembic](https://alembic.sqlalchemy.org/en/latest/) is used to handle database
migrations.

These migrations allow us to change the database schema without
removing and rebuilding the entire database. Instead, each migration has
an `upgrade` and a `downgrade` function. This way the migration can be
performed in both directions. For example, you might have a migration to add
a new table or column in the upgrade function, in the downgrade function you
would remove that new table or column.

Every migration also has a unique id and a reference to the previous migration.
This way alembic can figure out in which order to run the migrations to get
the database up to date. Alembic automatically creates a table containing the
id of the latest migration in the database, this way only the necessary
migrations are performed on startup.

Alembic automatically generates migrations by comparing the metadata from
`models.py` with the data currently in the database. Whenever the models have
changed you can generate a new migration by running:
```
$ bin/generate_migration.sh [container name] "[migration message]"
```

This script will start the specified container and the required database and
then run alembic to autogenerate a migration with the specified message.
You can see the message as a sort of commit message for migrations.

### Fixtures

Fixtures allow us to load known values into the database to be used for tests.
The auth service and db-setup service have a fixtures folder with .yml files
describing the entries that need to be added to the database.

Here's an example from `users.yml` in the auth service:
```
model: app.src.models.User
primary_key: ['id']

records:
  - id: 1
    email: "admin@uce.nl"
    password: "<password hash>"
    name: "admin"
    pin_group_id: 100
    auth_level: 4

```