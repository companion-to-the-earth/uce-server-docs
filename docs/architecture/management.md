# group management

This service is used to handle all the group management API requests. 
With groups we mean courses, classes and user-groups.
These three groups are all in the same service because they are not called too often. 

## Global structure
### Classes and files
The structure of the service is similar to most other microservices.

Here is an overview of the file structure:
```
/
├── Dockerfile          # Dockerfile to build the image
│
├── src/                # .py source files
│   ├── schemas/                            # bare schemas used by the auth service
│   ├── app.py                              # flask setup
│   ├── log.py                              # logging tool
│   ├── auth.py                             # handles internal requests to the auth service
│   ├── middleware.py                       # makes internal requests to the main database and authentication service
│   ├── user_groups_management.py           # handles the /user-groups/ requests
│   ├── classes_management.py               # handles the /classes/ requests
|   ├── courses_management.py               # handles the /courses/ requests
│   ├── query_user_groups.py                # creates sql queries for the user-group requests which are then send to the database
|   ├── query_classes.py                    # creates sql queries for the classes requests which are then send to the database
|   ├── query_courses.py                    # creates sql queries for the courses requests which are then send to the database
│   └── utils.py                            # utility functions
│
├── test_integration/   # integration test container data
│   ├── itest_group_management.py       # contains the integration tests
│   └── ...                             # other integration test files
│
├── test_unit/          # unit test data
│   └── utest_utils.py      # contains the unit tests
```

### Handling a request
A management request arrives at the [proxy](proxy.md) service.
The proxy redirects the request to the address of the management service container.
Flask matches the request route with the right routes defined in the scripts.
Then the function is called that belongs to the route as defined in the user_group_management.py, classes_management or courses_management.py.

Once the request has arrived at the containers these steps are roughly followed:

- Check if there is a [JWT token](auth.md#json-web-tokens) in the request
- Check if the request parameters/body are correct
- Check if provided values in the request are valid
- Create a sql query by calling the correct function in query.py
- The query is build by the function and send to the middleware-db
- The response of the middleware-db is then received
- The function in query.py parses the database response and returns it to user_group_management.py, classes_management or courses_management.py.
- A response is send back to the user

#### Example of a request cycle
This is a rough example of the path that a request makes:

## Requests
For more information on how to send the requests you can find a link to the openapi [here](API-reference.md).

#### User-groups
This Handles all the requests that are send to the api/user-groups/ routes. The following requests can be done:

- create user-groups with a POST request.
- get multiple user-groups with a GET request. You can get all user-groups that belong to universities, courses, classes or a user. You can define which user-groups you want to get with parameters in the request.
- get a specific user-group with a GET request. This request needs an user-group id and then returns all data that belongs to that user-group
- update the user-group information with a PUT request.
- delete a user-group with a DELETE request. the all users are then kicked from the group (so the entries from Users_per_user_group are also removed via delete on cascade). The pin_group is also removed.
- join a user-group with a GET request. An entry with the user_id and user_group_id is made in the database table Users_per_user_group. User can only join a group that is not full.
- leave a user-group with a GET request. user can leave the user-group that they are in with this request. 

#### classes
This Handles all the requests that are send to the api/classes/ routes. The following requests can be done:

- create class with a POST request.
- get multiple classes with a GET request. You can get all classes that belong to universities or courses. 
- get a specific class with a GET request.
- update the class information with a PUT request.
- delete a class. classes can only be removed if they are empty.

#### courses
This Handles all the requests that are send to the api/courses/ routes. The following requests can be done:

- Create a course with a POST request.
- Get multiple courses with a GET request. You can get all courses that belong to a university. 
- Get a specific course with a GET request.
- Update the course information with a PUT request.
- Delete a course. Courses can only be removed if they are empty.


## Errors
3 standard errors have been defined to give the user some feedback on their input,
and also to hide the inner workings of the server so we do not give too much internal information to people with bad intentions.

| Type                  | Message                                                       | Code  |
| :---------------------|:------------------------------------------------------------- | -----:|
| Bad request           | Invalid request                                               | 400   |
| Token error           | Token missing or invalid                                      | 401   |
| Unauthorized          | Not authorized to perform this action or access this resource | 403   |

<br>
The JWT token is always checked first, if it is invalid the request is rejected instantly without looking at the request body.
If the token is valid the request parameters and/or bodies are validated.
When the validation fails the server can return a bad request error, this depends on the type of request.
If the request body is correct an internal request will be made to the database through the middleware connector.
<br>
An error code 500 can occur but shouldn't. This means an internal server error. If this happens something isn't caught right on the server side. This should be fixed.

## Conversion of request parameters
The decision was made to try parsing the parameters to the right type at all cost.
We chose to do this because the front-end applications have a hard time supporting 64 bit integers and floating point numbers,
in this way the front-end application can send these numbers as a string which circumvents all these number related issues. For this reason we always send back id's as string and int. The front-end can then decide which to use. 

## ID's
Each user-group, class, course and university has a randomly generated int64 ID for a primary key.
When generating a random ID the database is checked for duplicate keys although the chance of that happening is very small.
If the randomly generated key is a duplicate a new one will be generated.

## Pin groups
Each user-group and class has its own pin group. users in this user-group or class can share pins with each other via this pin group. 

## TODO
This management service is not finished yet. We didn't have time to implement everything. Here are things that we still needed to implement.

### Authorization
Authorization still needs to be added to the requests. Because the requests only checks if the JWT is available and not if the JWT is valid. So basically every person can do everything. In the leave and join requests we already made an internal requests for the user info with the given JWT. You can use this to implement the authorization in the other requests. for more information about auth requests or example code you can go [here](auth.md).

### University requests
University requests are not implemented. We didn't really know if it was necessary because every university is probably going ot have its own server. But this can still be implemented if for some reason two universities want to have a shared server. This shouldn't be to hard so it is also good exercise to understand the server better. The requests look the same as classes and courses.

### Kick user-group request
There is no request to kick users form a user-group yet. so you have to change the LEAVE request or you have to make an new request so teacher can kick users from the teacher module.