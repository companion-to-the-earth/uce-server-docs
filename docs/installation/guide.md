# Installation guide

This guide describes how to set up the server for development or productions.
Additionally, there is a section about how to set up the CI/CD pipeline.

## Sections

### [Requirements](requirements.md)
Start here, this describes how to install the basic dependencies required
for all other steps.

### [Development setup](development_setup.md)
Guide on how to start the server locally for development.

### [Server setup](server_setup.md)
Guide on how to set up the server in production without the pipeline.

### [Pipeline setup](pipeline_setup)
Guide on how to set up the CI/CD pipeline and use it to automatically deploy the server.