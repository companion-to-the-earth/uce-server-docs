# Requirements

This guide describes how to install all dependencies necessary to run the server.
If you want to set up the CI/CD pipeline some extra dependencies are necessary,
these are described in the [pipeline setup](pipeline_setup.md). 

## Install Docker

As the docker version supplied by the default repository of your distro might
not be the latest version, please follow the
[docker installation guide](https://docs.docker.com/engine/install/)
for your distro to get the lastest docker version.

Afterwards, verify that docker is installed by running:
```
$ docker --version
```

Once docker is installed, start and enable the docker service:
```text
$ systemctl start docker
$ systemctl enable docker
```

You can verify if the service started successfully by running:
```
$ systemctl status docker
```

After docker is installed, install docker-compose from the official repositories:
```
$ yum install docker-compose
```

## Server setup

Now that all basic requirements are installed, you can continue with the server setup:

* [Development setup](development_setup.md) - local development environment
* [Server setup](server_setup.md) - production server setup
* [Pipeline setup](pipeline_setup.md) - RECOMMENDED, server setup with pipeline for automatic deployment
