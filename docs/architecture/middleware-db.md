# middleware-db
The middleware-db is a API for the database. When the middleware receives a
database query, this query is send to the middleware-db. The middleware-db sends
this query to the database. The database returns certain values upon
receiving the query, back to the middleware-db. The middleware-db then returns
it to the middleware so it can be sent back to the query sender.

## lib-database-connector
For communication with the database, [lib-database-connector](https://gitlab.com/companion-to-the-earth/lib-database-connector)
is used.
This is a simple python package that runs given queries on a database.
The supported queries are:

- Select
- Alter
- Insert
- Update
- Delete
- Create

A Select query will return the query result as expected. For the other queries
a value of 0 is returned if it was successful.
In case of an error, an error code and error message is returned.

## Internal API

{%
template "internal_api.html"
name: exit consume
desc: Closes the connection between the middleware and middleware-db
returns:
    -
        val: closed connection with middleware
        when: if the connection was closed successfully

example: "publish('middleware-db', 'exit consume', '{reply_queue}')"
%}

{%
template "internal_api.html"
name: query
desc: Closes the connection between the middleware and middleware-db
parameters:
    -
        name: query
        desc: the query to send to the database

returns:
    -
        val: "{query_result}"
        when: if query was successful
    -
        val: "ERROR {error_number}: {error_description}"
        when: if an error occurred

example: "publish('middleware-db', 'SELECT * FROM Universities WHERE University_ID = -1', '{reply_queue}')"
%}