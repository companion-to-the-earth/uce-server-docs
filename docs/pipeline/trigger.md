# Trigger

This step creates a list of all files that were changed. The list is later used
by the prepare step to create a list of images to rebuild.

The exact steps taken by this stage depend on what triggered the pipeline.

Additionally, if you want to force a full rebuild regardless of the changes you
can set the environment variable `FORCE_REBUILD` to 1 when starting a pipeline
manually in gitlab.

### Tags

For tags all files are seen as edited. This ensures a release always triggers a
full server rebuild.

### Merge request

!!!warning
    Currently the branch is always compared against the master branch, this should
    use `$CI_MERGE_REQUEST_TARGET_BRANCH_NAME` instead.

For merge requests the current branch is compared against the target branch of
the merge request. This ensures all changes made in the entire branch can be
tested.

### Push

For regular commits without an open merge request it is only compared agains the
previous commit. This means only the changes made in that commit are tested.