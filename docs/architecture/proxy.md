# proxy

For the proxy we use [nginx](https://www.nginx.com/).
This redirects all requests to the appropriate service and handles file
downloads.

Here is an overview of all the files used for the proxy:
```
proxy/
├── Dockerfile          # Dockerfile to build the image
│
├── letsencrypt/        # letsencrypt volume, mounted to /etc/letsencrypt. Must be in .gitignore
│
├── nginx/              # nginx configuration, copied to /etc/nginx/
│   ├── nginx.conf      # main nginx config file
│   ├── include/        # shared nginx configuration files, can be included anywhere
│   └── templates/      # template files, copied to /etc/nginx/conf.d after envsubst
│
├── scripts/                    # any scripts used in the container
│   └── start_nginx_certbot.sh  # startup script
│
└── static/             # static files, reachable via /static route
```

## Configuration
All configuration files are located in `proxy/nginx/` in the
[uce-server](https://gitlab.com/companion-to-the-earth/uce-server) repository.
These files are all copied to `/etc/nginx/` in the container.

In `proxy/nginx/templates/` are template files for the nginx configuration.
This is a feature of the docker nginx image, all .template files in this
directory are copied to `/etc/nginx/conf.d/` after substituting all environment
variables. This way we can make the domain name configurable in the `.env` file.

Files that can be included in any other configuration file can be found in
`proxy/nginx/include/`. These files can be included in any config file using:
```
include /etc/nginx/include/<filename>.conf;
```

### nginx.conf
`nginx.conf` is the main configuration file.
All basic settings are configured here. Every option has a comment explaining
what the option does and more information can be found in the
[nginx documentation](http://nginx.org/en/docs/).

This file also includes all files in `conf.d/`.

### templates/uce-server.conf.template

This file contains all the configuration necessary to forward requests to the
appropriate services.

#### Example configuration
!!!warning
    If you need to edit this file, make sure to carefully check the nginx
    documentation on `location` blocks as trailing slashes are important.
    `example.com/some/path` is not the same as `example.com/some/path/`.
```
server {
    listen 80;                  # ipv4
    listen [::]:80;             # ipv6
    server_name ${DOMAIN_NAME}; # get domain name from environment variable

    # Static content
    location /static {
        root /var/www;   # nginx will expand this to /var/www/static as the location is /static
        index index.html;
    }

    # API endpoints
    location /api {

        include /etc/nginx/include/cors.conf; # include CORS headers

        # -AUTH-
        location /api/auth/ {
            include /etc/nginx/include/cors-preflight.conf; # handle CORS preflight requests
            limit_except GET POST DELETE OPTIONS { deny all; }
            proxy_pass http://auth:5000/;
        }
    }
}

```

Note that the address provided with `proxy_pass` uses the container name.
Docker compose automatically sets up a virtual network with all containers
listed in `docker-compose.yml`. Within this network, containers are reachable
by their name.
As all Flask-based services listen on port 5000 we send the request to port
5000 of the `auth` container.

Another thing to note is that the ports specified in the beginning are
ports on the container, not the host machine. The docker-compose file includes
the following lines:
```
ports:
  - "${PROXY_PORT}:80"
  - "${PROXY_PORT_SSL}:443"
```
This means the port set in the environment variable `PROXY_PORT` on the host
machine is forwarded to port 80 on the proxy container. For example: if
`PROXY_PORT` is set to 8080, all requests sent to the host machine at port 8080
will be forwardedy to the proxy container at port 80.

### include/cors.conf
CORS (Cross-Origin Resource Sharing) is used to specify which origins are
allowed to read information from a web browser.

For example:
Code served from the teacher module needs to make a request for some resource
on the server. As the server is hosted on a different domain, this is a
cross-origin HTTP request which browsers restrict by default.
The CORS headers specify which domains are allowed to access a resource
so the browser will allow the request.

More information on why this is required and how to set it up can be found
[here](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

### Example configuration
```
###
# CORS
###

# the domains that are allowed to send requests
add_header Access-Control-Allow-Origin '*' always;
# the methods that are allowed
add_header Access-Control-Allow-Methods 'GET, POST, PUT, PATCH, DELETE, OPTIONS' always;
# the request headers that are allowed
add_header Access-Control-Allow-Headers '*' always;
# the response headers that are allowed
add_header Access-Control-Expose-Headers '*' always;
# enable/disable cookies
add_header Access-Control-Allow-Credentials 'true' always;
```

### include/cors-preflight.conf
This file ensures CORS preflight requests are handled correctly.
Before sending the actual request a browser might send a preflight request.
This is an http `OPTIONS` request. The response is 204 No Content, with all
CORS headers included so the browser can determine if it is safe to send the
actual request.

### Example configuration
```
##
# Handle CORS preflight requests, CORS headers are set in cors.conf
##

if ($request_method = OPTIONS) {
    include /etc/nginx/include/cors.conf;                # include CORS headers
    add_header Access-Control-Max-Age 1728000;           # valid for 20 days
    add_header Content-Type "text/plain; charset=utf-8";
    add_header Content-Length 0;
    return 204;
}
```

## Certbot

To automatically request and renew SSL certificates we use
[certbot](https://certbot.eff.org/). This allows us to automatically request
Let's Encrypt certificates and will also automatically renew them.

The `.env` file contains a few environment variables that need to be set to be
able to use this.

First, `USE_CERTBOT` should be set to 1 so certbot is started inside the proxy
container.

Next, `DOMAIN_NANE` needs to be set to the domain name used for the server.
This will be the common name on the SSL certificate.

Finally, `CERTBOT_EMAIL` should be set to a valid email address. While this step
is optional, it is highly reccommended as this will be used for important
account notifications.

!!!note
    Let's Encrypt has [rate limits](https://letsencrypt.org/docs/rate-limits/)
    so, if you need to test things, consider adding the `--staging` flag to the
    certbot command in `proxy/scripts/start_nginx_certbot.sh`. This will use the
    Let's Encrypt staging server which provides fake certificates for testing
    purposes.

If these settings are configured correctly, when the proxy container is started
certbot will request a certificate. Additionally, a cron job is set up to renew
the certificate if necessary. This job is executed daily.

All Let's Encrypt files can be found in `proxy/letsencrypt/` after running the
container with certbot enabled. This is a volume mounted at `/etc/letsencrypt/`
in the container. This directory must be ignored by git as it contains the
requested certificate and private key used by the server. This is to ensure any
existing certificate can be reused if the container is restarted.
