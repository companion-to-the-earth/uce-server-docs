# db-auth
This database only contains the tables necessary for authentication and
authorization.
The auth service is the only service that has a connection to this database.

## Tables
This are all the tables in the main database. *PK* means Primary Key,
*FK* Foreign Key and *U* UNIQUE. Tables are connected trough the *FK's*.

#### users
!!!note
    The **class_id** column can be null but this is only supposed to be the case
    for admins. All other users have to be in a class.
{%
template "db.html"
attributes:
    -
        name: id
        type: BIGINT
        pk: True
        constraint: NOT NULL
    -
        name: email
        type: VARCHAR(320)
        unique: True
        constraint: NOT NULL
    -
        name: password
        type: CHAR(60)
        constraint: NOT NULL
    -
        name: name
        type: VARCHAR(50)
        constraint: NOT NULL
    -
        name: class_id
        type: BIGINT
        constraint: "NULL"
    -
        name: pin_group_id
        type: BIGINT
        constraint: NOT NULL
    -
        name: auth_level
        type: INT
        constraint: NOT NULL
    -
        name: auth_time
        type: TIMESTAMP
        constraint: "NULL"
%}

#### refresh_tokens
{%
template "db.html"
attributes:
    -
        name: session_id
        type: BIGINT
        pk: True
        constraint: NOT NULL
    -
        name: user_id
        type: BIGINT
        fk: True
        constraint: NOT NULL
    -
        name: token
        type: VARCHAR(100)
        unique: True
        constraint: NOT NULL
    -
        name: issued_at
        type: TIMESTAMP
        constraint: NOT NULL
    -
        name: invalid
        type: BOOLEAN
        constraint: NOT NULL
%}

#### password_reset_tokens
{%
template "db.html"
attributes:
    -
        name: token
        type: VARCHAR(100)
        pk: True
        constraint: NOT NULL
    -
        name: user_id
        type: BIGINT
        fk: True
        constraint: NOT NULL
    -
        name: expires
        type: TIMESTAMP
        constraint: NOT NULL
%}

#### dummy_users
{%
template "db.html"
attributes:
    -
        name: token
        type: VARCHAR(100)
        pk: True
        constraint: NOT NULL
    -
        name: email
        type: VARCHAR(320)
        constraint: NOT NULL
    -
        name: auth_level
        type: INT
        constraint: NOT NULL
    -
        name: class_id
        type: BIGINT
        constraint: NOT NULL
    -
        name: user_group_id
        type: BIGINT
        constraint: "NULL"
    -
        name: created_on
        type: TIMESTAMP
        constraint: NOT NULL
%}

## Architecture
The database architecture of the auth database.
![](../images/db_auth.svg)