# Package

This job is used for python packages.

It builds all packages listed in `.config/.packages` and uploads them to the
gitlab package registry.


## Usage

For this job a `setup.py` file is required in the package directory.

Additionally, `.config/.packages` should contain a list of packages to build.

Finally, the package step should be included in `.gitlab-ci.yml` like so:
```
include:
  - project: *project
    ref: *version
    file: '.gitlab-ci.package.yml'
```

Here's an example setup for
[lib-middleware-connector](https://gitlab.com/companion-to-the-earth/lib-middleware-connector):

### setup.py
```
import setuptools

setuptools.setup(
    name="middleware-connector",
    version="2.0.4",
    description="AMQP client library",
    packages=setuptools.find_packages(where="src"),
    package_dir={"":"src"},
    install_requires=[
        "pika~=1.2.0"
    ],
    python_requires=">=3.8",
)
```

### .config/.packages
```
middleware-connector
```

## Using packages from gitlab package registry

To use packages from a certain package registry, add the following line at the
top of the `requirements.in` file:
```
--extra-index <package index url>
```

To get the package index url, check the
[gitlab documentation](https://docs.gitlab.com/ee/user/packages/pypi_repository/index.html#authenticate-with-the-package-registry).
At the time of writing this the format is:
```
https://gitlab.com/api/v4/projects/<project_id>/packages/pypi
```
The project id can be found on the project's home page.

Once the extra index has been added you can add dependencies just like any other
package.
