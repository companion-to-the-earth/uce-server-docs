# pins

This service is used to handle the pin API requests.
With this service users are able to create, retrieve, update, share and delete pins.

## Global structure
### Classes and files
The structure of the service is similar to most other microservices.

Here is an overview of the file structure:
```
pins/
├── Dockerfile          # Dockerfile to build the image
│
├── content/            # pin content files are stored here
│
├── src/                # .py source files
│   ├── schemas/            # bare schemas used by the auth service
│   ├── app.py              # flask setup
│   ├── errors.py           # defines the error types
│   ├── log.py              # logging tool
│   ├── middleware.py       # makes internal requests to the main database and authentication service
│   ├── pincontent.py       # handles the /{pin_id}/contents/ requests
│   ├── pins.py             # handles the /pins/ requests
│   ├── query.py            # creates sql queries which are to be send to the database
│   └── utils.py            # utility functions
│
├── test_integration/   # integration test container data
│   ├── itest_pins.py       # contains the integration tests
│   └── ...                 # other integration test files
│
├── test_unit/          # unit test data
│   └── utest_utils.py      # contains the unit tests
```

### Handling a request
A pin request arrives at the [proxy](proxy.md) service.
The proxy redirects the request to the address of the pin service container.
The request is handled as defined in the pins.py or pincontent.py.
Each function which can be called from an API request has a route, these routes are handled by Flask.

Once the request has arrived at the pin container these steps are roughly followed:

- Validate the [JWT token](auth.md#json-web-tokens)
- Check if the request parameters/body are correct
- Create a sql query by calling the correct function in query.py
- The query is internally send to the database via `publish_message()`
- The database response is read by calling `get_thread_response()`
- The function in query.py parses the database response and returns it to pins.py or pincontent.py
- A response is send

#### Example of a request cycle
This is a rough example of the path that a request makes:
![](../images/pin_request_example.png)

### Pin content
The pins services handles uploading, downloading, editing and removing of pin content.
Pin content is stored in pins/content/.

#### Structure
The structure of a pin content request is very similar to the normal pin requests.
The Flask routes of the pin content are defined in pincontent.py
The only difference is that the pin content requests end with some file alteration calls.
Files are stored in `pins/content/` and are mounted in the container at `/var/www/content/`

#### Allowed file types
Due to security reasons we only allow files to be of type image/png, image/jpg, image/jpeg, video/mp4 or text/plain as defined in `ALLOWED_EXTENSIONS`.

#### Pin content ID's
Pin content id's are 64 bit integers generated the same way as pin id's.
The files are given the id as name, this has multiple advantages:<br>
We do not have to worry about duplicate names because the id's are primary keys thus are unique.<br>
We do not have to store the filename seperately in the database.

## Authorization levels
Different [authorization levels](auth.md#auth-levels) allow for different possible actions on the server.

There are 4 authorization levels with the following privileges:

#### 1 (student)
Students can create, edit, delete and share their personal made pins.
They can get their personal pins, group pins and class pins (getting group/class pins or sharing pins is only allowed if the student is part of that group/class).

#### 2 (teacher assistent)
The ta's have the same privileges as the students regarding pins.

#### 3 (teacher)
Teachers can create personal pins. Teachers can get/delete/edit/share any pin they want.

#### 4 (Admin)
Admins have the same privileges as teachers regarding pins.

## Errors
4 standard errors have been defined to give the user some feedback on their input,
and also to hide the inner workings of the server so we do not give too much internal information to people with bad intentions.

| Type              | Message                                                       | Code  |
| :---------------- |:------------------------------------------------------------- | -----:|
| Bad request       | Invalid request                                               | 400   |
| Value not found   | Value not found                                               | 400   |
| Token error       | Token missing or invalid                                      | 401   |
| Unauthorized      | Not authorized to perform this action or access this resource | 403   |

<br>
The JWT token is always checked first, if it is invalid the request is rejected instantly without looking at the request body.
If the token is accepted the request parameters and/or bodies are validated.
When the validation fails the server can return a value not found or bad request error, this depends on the type of request.
If the request body is correct an internal request will be made to the database through the middleware connector.
<br>
The response from the database which the user gets to see can be one of 3 things:

- Succes
- Unauthorized
- Value not found

## Conversion of request parameters
The decision was made to try parsing the parameters to the right type at all cost.
We chose to do this because the front-end applications have a hard time supporting 64 bit integers and floating point numbers,
in this way the front-end application can send these numbers as a string which circumvents all these number related issues.


## Pin/Pincontent ID's
Each pin has a randomly generated int64 ID for a primary key.
When generating a random ID the database is checked for duplicate keys although the chance of that happening is very small.
If the randomly generated key is a duplicate a new one will be generated.
