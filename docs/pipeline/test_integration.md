# Test_integration

This stage runs the integration tests for a service.
The integration tests test the entire service, to do this extra services might
need to be started if the service depends on them.
These tests make up most of the tests.
Unlike unit tests, all the services running the integration tests are running in
a docker container using docker in docker. This means that for each integration
test run (the entire pipeline job, not the individual services) you have a clean
environment.

## Writing integration tests

Just like the unit tests, the integration tests are written using the unittest
library.
The steps required to add integration tests are a little more involved though.
First, create a `test_integration` directory in the image.
Next, a `docker-compose.test.yml` file is required, this handles startup of the
integration test container.

Here's an example `docker-compose.test.yml`:
```
version: '3'

services:
  auth-test-integration:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/auth:$CI_COMMIT_REF_NAME-$STAGE
    container_name: auth-test-integration
    environment:
      WAIT_HOSTS: "auth:5000, middleware:5672, pins:5000, group_management:5000, mail:5000"
    volumes:
      - ./auth/test_integration:/server/framework/app/test_integration
    command: sh -c "/wait; python -m unittest discover /server/framework/app -p 'itest_*.py'"
    depends_on:
      - auth
      - db-setup
      - pins
      - group_management
      - mail
```

All services that need to be started for the tests to run should be specified in
`depends_on:`. This list should also contain the service that's being tested, as
the integration tests run in a seperate container.
If the service you want to test contains the
[docker-compose-wait](https://github.com/ufoscout/docker-compose-wait) script
you can use the environment variable `WAIT_HOSTS` to wait until the required
services have finished starting.

Note that the actual integration tests are mounted as a volume in the container,
this way the integration tests don't need to be copied into the image.

Finally, the actual tests can be written in files named `itest_*.py`.

## Exit code 137

This is a common cause of failing integration tests. Most of the time this
happens with the middleware service as it sometimes needs a lot of time to shut
down. When the tests are done and all services are stopped, sometimes the
middleware container needs longer than the timeout value to shut down. When this
happens, docker will immediately kill the container causing it to exit with code
137.