# db
In this database we store pins and their content, as well as the university, class and group the users belong to. Only the database API has a connection with the database. The database uses MySQLdb, and subsequently mysql. Furthermore, maps are stored in the database when uploaded from the teacher module. Lastly, when a new user is registered, a verification code is stored on the database to be mailed to the user to validate their account.

## Tables
This are all the tables in the main database. *PK* means Primary Key and *FK* Foreign Key. Tables are connected trough the *FK's*.
#### Universities
{%
template "db.html"
attributes:
    -
        name: UniversityID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Name
        type: VARCHAR(30)
        constraint: NOT NULL
%}

#### Classes
{%
template "db.html"
attributes:
    -
        name: ClassID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Name
        type: VARCHAR(30)
        constraint: NOT NULL
    -
        name: UniversityID
        fk: True
        type: BIGINT(16)
        constraint: NOT NULL
    -
        name: PinGroupID
        fk: True
        type: BIGINT(16)
        constraint: NOT NULL
%}

#### User_Groups
{%
template "db.html"
attributes:
    -
        name: UserGroupID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Name
        type: VARCHAR(30)
        constraint: NOT NULL
    -
        name: ClassID
        type: BIGINT(16)
        fk: True
        constraint: NOT NULL
    -
        name: PinGroupID
        type: BIGINT(16)
        fk: True
        constraint: NOT NULL
%}

#### Users_Per_User_Group
{%
template "db.html"
attributes:
    -
        name: UserID
        type: CHAR(36)
        pk: True
        fk: True
        constraint: NOT NULL
    -
        name: UserGroupID
        type: CHAR(36)
        fk: True
        constraint: NOT NULL
%}

#### Pins
{%
template "db.html"
attributes:
    -
        name: PinID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Title
        type: VARCHAR(50)
        constraint: NOT NULL
    -
        name: LastEdited
        type: BIGINT(16)
        constraint: NOT NULL
    -
        name: Latitude
        type: DOUBLE PRECISION
        constraint: NOT NULL
    -
        name: Longitude
        type: DOUBLE PRECISION
        constraint: NOT NULL
    -
        name: UserID
        type: BIGINT(16)
        constraint: NOT NULL
    -
        name: text_content
        type: TEXT
%}

#### PinContent
{%
template "db.html"
attributes:
    -
        name: ContentID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: PinID
        type: BIGINT(16)
        fk: True
        constraint: NOT NULL
    -
        name: ContentNr
        type: INT
        constraint: NOT NULL
    -
        name: Content
        type: VARCHAR(255)
        constraint: NOT NULL
    -
        name: Type
        type: ENUM(text,image,video,quiz)
        constraint: NOT NULL
%}

#### Pins_Per_Pin_Group
{%
template "db.html"
attributes:
    -
        name: PinID
        type: BIGINT(16)
        cpk: True
        fk: True
        constraint: NOT NULL
    -
        name: PinGroupID
        type: BIGINT(16)
        cpk: True
        fk: True
        constraint: NOT NULL
%}
PinID and PinGroupID are a combined primary key so PK is (PinID, PinGroupID)

#### Pin_Groups
{%
template "db.html"
attributes:
    -
        name: PinGroupID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Name
        type: VARCHAR(50)
        constraint: NOT NULL
%}

#### User_Location_Histories
{%
template "db.html"
attributes:
    -
        name: UserID
        type: BIGINT(16)
        pk: True
        constraint: NOT NULL
    -
        name: Location
        type: VARCHAR(22)
        constraint: NOT NULL
    -
        name: LocationTime
        type: BIGINT(16)
        constraint: NOT NULL
%}

#### Verification_Codes
{%
template "db.html"
attributes:
    -
        name: Verification_Code
        type: CHAR(36)
        pk: True
        constraint: NOT NULL
    -
        name: Expiration_Date
        type: TIMESTAMP
        constraint: NOT NULL
    -
        name: UserID
        type: BIGINT(36)
        constraint: NOT NULL
%}

## Architecture
The database architecture of all the databases.
![](../images/db_server.svg)